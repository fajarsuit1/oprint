<div class="about" id="about">
		<div class="agileits_works-top">
				<div class="col-md-4 about-img-right">
					
				</div>
				<div class="col-md-8 agileits_works-grid two">
				 
				    <div class="wthree-text">
					<div class="heading-setion-w3ls">
						<center><h3 class="title-w3layouts">Online Printing <i class="fa fa-bell-o" aria-hidden="true"></i><i class="fa fa-bell" aria-hidden="true"></i></h3></center>
					</div>
					
					<img src="<?php echo base_url(); ?>aset/images/logo.png" height="270px" width="450px">
					   
					   <ul class="about-agile">
						<li><i class="fa fa-check-square fa-2x" aria-hidden="true"></i>Apa Itu Online Printing ?</li>
						<p class="para-w3-agile">Online Printing adalah sebuah perusahaan yang bergerak dibidang percetakan secara online. Online Printing menawarkan beberapa layanan yang tentunya mempermudah user, karena untuk mencetak file user tidak perlu repot repot datang ke tempat percetakan cukup mengupload file melalui website online printing dan dokumen pun akan segera dicetak. tidak hanya itu online printing juga menyediakan jasa antar barang  dengan sistem pembayaran "Bayar Ditempat".</p>
						<li><i class="fa fa-check-square fa-2x" aria-hidden="true"></i>Bagaimana Catatan Sejarah Berdirinya Online Printing ?</li>
						<p class="para-w3-agile">Usaha ini berdiri sejak tahun 2018, dimana awal mulanya berawal dari tugas kuliah sekelompok mahasiswa yang kemudian mereka memiliki keberanian untuk mengembangkan tugas mereka menjadi sebuah usaha. Banyak rintangan yang dialami ketika mendirikan usaha ini tetapi dengan semangat dan kebulatan tekad mereka akhirnya perusahaan ini berdiri sampai sekarang.</p>
						<li><i class="fa fa-check-square fa-2x" aria-hidden="true"></i>Apa kelebihan dari Online Printing ?</li>
						<p class="para-w3-agile">Kelebihaannya adalah Online Printing ini sangat mempermudah dan memanjakan user dalam masalah percetakan. Dimana user tidak perlu repot repot datang ke tempat peretak untuk mencetak filenya. Cukup mengupload file lewat web lalu file tersebut akan dicetak, tidak hanya itu online printing juga melayani jasa antar untuk wilayah surabaya. Sehingga user cukup duduk manis di rumah, metode pembayarannya pun menggunakan sistem bayar ditempat, yang membuat user tidak perlu repot repot untuk mentransfer uang lewat bank maupun atm. </p>
						<li><i class="fa fa-check-square fa-2x" aria-hidden="true"></i>Dimana Lokasi Online Printing ?</li>
						<p class="para-w3-agile">Perusahaan ini berlokasi di gang lebar, Kelurahan Wonocolo Surabaya, yang lebih tepatnya di belakang kampus UIN Sunan Ampel Surabaya. Lokasinya sangat strategis karena dekat dengan tempat tinggal para mahasiswa maupun mahasiswi Kampus UINSA. </p>
					</ul>
					</div>
					
				</div>
				
		    <div class="clearfix"> </div>
		</div>
		
		<div class="agileits_work_grids">
			<ul id="flexiselDemo1">			
				<li>
					<div class="agileits_work_grid view view-sixth">
						<img src="images/g1.jpg" alt=" " class="img-responsive" />
						<div class="mask">
							<a href="buynow.html" class="info">Buy Now</a>
						</div>
					</div>
				</li>
				<li>
					<div class="agileits_work_grid view view-sixth">
						<img src="images/g2.jpg" alt=" " class="img-responsive" />
						<div class="mask">
							<a href="buynow.html" class="info">Buy Now</a>
						</div>
					</div>

				</li>
				<li>
					<div class="agileits_work_grid view view-sixth">
						<img src="images/g3.jpg" alt=" " class="img-responsive" />
						<div class="mask">
							<a href="buynow.html" class="info">Buy Now</a>
						</div>
					</div>
				</li>
				<li>
					<div class="agileits_work_grid view view-sixth">
						<img src="images/g4.jpg" alt=" " class="img-responsive" />
						<div class="mask">
							<a href="buynow.html" class="info">Buy Now</a>
						</div>
					</div>
				</li>
				<li>
					<div class="agileits_work_grid view view-sixth">
						<img src="images/g5.jpg" alt=" " class="img-responsive" />
						<div class="mask">
							<a href="buynow.html" class="info">Buy Now</a>
						</div>
					</div>
				</li>
			</ul>
	
		</div>
	</div>
<!-- //about -->
<!-- team -->

<!-- //team -->
<!-- services -->
	\
	<!-- //services -->
