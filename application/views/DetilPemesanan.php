<html lang="en">
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="bs-example">
    <h1 style='text-align: center;'>Detil Pemesanan</h1>
    <form  class="form-horizontal" <?php echo form_open_multipart('DetilPemesanan/aksi_upload');?>>
    <input type="hidden" name="quantity" value="pengguna">
    <div class="form-group">
        <input type="hidden" name="id" value="<?php echo $id ?>">
        <input type="hidden" name="id_jenis" value="<?php echo $id_jenis ?>">
        <input type="hidden" name="quantity" value="<?php echo $quantity ?>">
         <input type="hidden" name="estimasi_harga" value="<?php echo $harga ?>">

        <div class="form-group">
            <label class="control-label col-xs-2" for="Nama">Nama Penerima:</label>
            <div class="col-xs-8">
                <input type="text" name="nama_penerima" class="form-control" id="Namapenerima" placeholder="Nama Penerima">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2" for="Alamat">Alamat Penerima:</label>
            <div class="col-xs-8">
                <textarea rows="8" name="alamat_kirim" class="form-control" id="Alamat" placeholder="Masukan Alamat Lengkap Tujuan Pngiriman"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2" for="Nama">Estimasi Harga:</label>
            <div class="col-xs-8">
                <input type="text" name="estimasi_harga" class="form-control" id="Estimasiharga" placeholder="Estimasi Harga" disabled value="<?php echo $harga ?>">
            </div>
        </div>
         <div class="form-group">
            <label class="control-label col-xs-2" for="foto">Unggah Dokumen:</label>
            <div class="col-xs-8">
                <input type="file" name="userfile">
            </div>
        <br>
        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-8">
                <input type="submit" class="btn btn-primary" value="Kirim">
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
        </div>
    </form>
</div>
</body>
</html>                            